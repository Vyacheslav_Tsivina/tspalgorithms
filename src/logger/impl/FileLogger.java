package logger.impl;

import logger.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import static util.Constants.DATE_FORMAT;

public class FileLogger implements Logger {
    private File file;
    private BufferedWriter bw;
    public FileLogger(File file) throws IOException {
        this.file = file;
        bw = new BufferedWriter(new FileWriter(file));
    }


    @Override
    public void logBooleanMatrix(boolean[][] matrix) {
        try {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    bw.write(String.format("%5.b", matrix[i][j]));
                }
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logDoubleMatrix(double[][] matrix) {
        try{
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    bw.write(String.format("%5.f", matrix[i][j]));
                }
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logPath(int[] path, double pathWeight) {
        try{
            bw.write(DATE_FORMAT.format(new Date())+" Path weight: " + pathWeight);
            bw.newLine();
            bw.write("Path");
            bw.newLine();
            StringBuilder message = new StringBuilder();
            message.append("[");
            message.append(path[0]);
            for (int i = 1; i < path.length; i++) {
                message.append(", " + path[i]);
            }
            message.append("]");
            bw.write(message.toString());
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logString(String message) {
        try{
            bw.write(DATE_FORMAT.format(new Date())+" "+message);
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeBuffer(){
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
