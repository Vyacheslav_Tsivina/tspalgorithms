package logger.impl;

import logger.Logger;

public class ConsoleLogger implements Logger {
    @Override
    public void logPath(int[] path, double pathWeight) {
        System.out.println("Path weight: "+pathWeight);
        System.out.println("Path:");
        StringBuilder message = new StringBuilder();
        message.append("[");
        message.append(path[0]);
        for (int i = 1; i < path.length; i++) {
            message.append(", " + path[i]);
        }
        message.append("]");
        System.out.println(message.toString());
        System.out.println();
    }

    @Override
    public void logBooleanMatrix(boolean[][] matrix) {
        for (int i=0; i< matrix.length; i++){
            for (int j=0; j< matrix[0].length ; j++){
                System.out.print(String.format("%6b", matrix[i][j]));
            }
            System.out.println();
        }
    }

    @Override
    public void logDoubleMatrix(double[][] matrix) {
        for (int i=0; i< matrix.length; i++){
            for (int j=0; j< matrix[0].length ; j++){
                System.out.print(String.format("%5.f", matrix[i][j]));
            }
            System.out.println();
        }
    }

    @Override
    public void logString(String message) {
        System.out.println(message);
    }
}
