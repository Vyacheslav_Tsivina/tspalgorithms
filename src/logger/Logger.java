package logger;

import util.Constants;

public interface Logger {
    void logPath(int[] path, double pathWeight);

    void logString(String message);

    default void logDebug(String message) {
        if (Constants.LOG_LEVEL.equals(Constants.LogLevel.DEBUG)) {
            logString(message);
        }
    }

    void logDoubleMatrix(double[][] matrix);

    void logBooleanMatrix(boolean[][] matrix);
}
