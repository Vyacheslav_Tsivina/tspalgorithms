package algorithms;

import beans.Cycle;
import beans.IterationResult;
import beans.Report;
import logger.Logger;
import beans.Graph;
import util.CommonGraphOperations;
import util.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.lang.Math.*;
import static util.AntColonyConstants.*;


public class AntColonyAlgorithm {
    private Logger logger;
    private boolean[][][] tabooArray = new boolean[ANTS_NUMBER][][];
    private int[][] antPath = new int[ANTS_NUMBER][];
    private Graph pheromoneGraph;
    private Graph graph;
    private Report report;
    private int count;
    private long time;

    public AntColonyAlgorithm(Logger logger) {
        this.logger = logger;
        report = new Report();
        count = 0;
        time = 0;
    }


    public Report run(Graph graphToRun) {
        setReportConstants(graphToRun);
        long startTime = new Date().getTime();
        graph = graphToRun;
        init(graph);
        double bestWayLength = Double.MAX_VALUE;
        int[] bestWay = new int[graph.getN() + 1];
        for (int iteration = 0; iteration < ITERATIONS_COUNT; iteration++) {
            initFirstCity();
            /* TOUR STAGE*/
            for (int antIndex = 0; antIndex < ANTS_NUMBER; antIndex++) {
                antTravel(antIndex);

            }//TOUR
            bestWayLength = updatePheromoneLevel(bestWayLength, bestWay, iteration);
            saveIterationResult(bestWay, iteration);
            if (new Date().getTime() - startTime > WAIT_UNTIL_STOP) {//each graph has no more than hour
                report.getConstants().put("INTERRUPTED", "TRUE");
                break;
            }

        }
        printFinalResult(startTime, bestWayLength, bestWay);
        return report;
    }


    public Report runParallel(Graph graphToRun) throws Exception {
        setReportConstants(graphToRun);
        List<Double> pheromoneLevel = new LinkedList<>();
        long startTime = new Date().getTime();
        graph = graphToRun;
        init(graph);
        double bestWayLength = Double.MAX_VALUE;
        int[] bestWay = new int[graph.getN() + 1];
        for (int iteration = 0; iteration < ITERATIONS_COUNT; iteration++) {
            initFirstCity();
            ExecutorService service = Executors.newFixedThreadPool(8);//+- number of processor core number
            List<Future<Boolean>> endTravelMarks = new ArrayList<>(ANTS_NUMBER);
            /* TOUR STAGE*/
            for (int antIndex = 0; antIndex < ANTS_NUMBER; antIndex++) {
                endTravelMarks.add(service.submit(new AntTravelCallable(antIndex)));
            }//TOUR
            service.shutdown();
            for (int antIndex = 0; antIndex < ANTS_NUMBER; antIndex++) {
                endTravelMarks.get(antIndex).get();//wait until all task will be done
            }
            bestWayLength = updatePheromoneLevel(bestWayLength, bestWay, iteration);
            if (Constants.ADDITIONAL_INFO) {
                double allPheromone = 0;
                for (int i = 0; i < graphToRun.getN(); i++)
                    for (int j = 0; j < graphToRun.getN(); j++) {
                        allPheromone += pheromoneGraph.getEdge(i, j);
                    }
                double bestWayPheromone = 0;
                for (int i = 0; i < graphToRun.getN(); i++) {
                    bestWayPheromone += pheromoneGraph.getEdge(bestWay[i], bestWay[i + 1]);
                }
                pheromoneLevel.add(bestWayPheromone / allPheromone);
            }

            saveIterationResult(bestWay, iteration);
        }
        if (Constants.ADDITIONAL_INFO) {
            report.getConstants().put("PHEROMONE_AMOUNT", pheromoneLevel);
        }
        printFinalResult(startTime, bestWayLength, bestWay);
        System.out.println("TIME = "+time);
        return report;
    }

    private void printFinalResult(long startTime, double bestWayLength, int[] bestWay) {
        long endTime = new Date().getTime();
        logger.logString("----------------------------------------------------------------------");
        logger.logString("Run parallel method execution time:" + (endTime - startTime) + " ms");
        logger.logPath(bestWay, bestWayLength);
        report.setTime(endTime - startTime);
    }

    /**
     * Simulate ant travelling according to probability function
     * return true only as a mark for parallel running
     */
    private boolean antTravel(int antIndex) {
        for (int step = 1; step < graph.getN(); step++) {
            int currentCity = antPath[antIndex][step - 1];
            /**
             * Array contains end point for probabilities located on [0,1] segment one after another
             * For example:
             * probabilities = {{ 0, 0.1, 0.2}, {0.2 , 0, 0.3}, {0.1, 0.1, 0 }}
             * => probabilityArray = {0 , 0.1, 0.4, 0.5, 0.5, 0.8, 0.9, 1, 1}
             */
            double[] probabilityArray = computeProbabilityArray(antIndex, currentCity);

            /* Edge selection*/
            int edgeIndex = binarySearch(probabilityArray, random() * graph.getN());//select by random number from [0,1) interval
            saveMarks(edgeIndex, currentCity, antIndex, step);
        }
        return true;
    }

    /**
     * Randomly choose first city for each ant
     */
    private void initFirstCity() {
        for (int antIndex = 0; antIndex < ANTS_NUMBER; antIndex++) {
            for (int i = 0; i < graph.getN(); i++)
                for (int j = 0; j < graph.getN(); j++)
                    tabooArray[antIndex][i][j] = false;
            antPath[antIndex][0] = (int) (graph.getN() * random());
            for (int j = 0; j < graph.getN(); j++) {
                tabooArray[antIndex][j][antPath[antIndex][0]] = true;
            }
        }
    }

    /**
     * Updates pheromone graph according to antPath array
     * Updates bestWay array
     */
    private double updatePheromoneLevel(double bestWayLength, int[] bestWay, int iteration) {
    /*UPDATE PHEROMONE LEVEL STAGE*/
        double[][] newPheromone = new double[graph.getN()][graph.getN()];
        for (int antIndex = 0; antIndex < ANTS_NUMBER; antIndex++) {
            double pathLength = 0;
            for (int i = 0; i < graph.getN() - 1; i++) {
                pathLength += graph.getEdge(antPath[antIndex][i], antPath[antIndex][i + 1]);
            }
            pathLength += graph.getEdge(antPath[antIndex][graph.getN() - 1], antPath[antIndex][0]);//close cycle;
            for (int i = 0; i < graph.getN() - 1; i++) {
                newPheromone[antPath[antIndex][i]][antPath[antIndex][i + 1]] += Q / pathLength;
            }
            newPheromone[antPath[antIndex][graph.getN() - 1]][antPath[antIndex][0]] += Q / pathLength;
            if (pathLength < bestWayLength) {
                saveBestWay(bestWay, antPath[antIndex]);
                bestWayLength = pathLength;
                CommonGraphOperations.logNewBestCycle(bestWay, iteration, bestWayLength, logger, graph);
            }
        }
        for (int i = 0; i < graph.getN(); i++) {
            for (int j = 0; j < graph.getN(); j++) {
                double oldPheromoneLevel = pheromoneGraph.getEdge(i, j);
                pheromoneGraph.setEdge(i, j, (1 - P) * oldPheromoneLevel + newPheromone[i][j]);
            }
        }
        return bestWayLength;
    }

    /**
     * Method to init pheromoneGraph and tabooArray
     *
     * @param graph
     */
    private void init(Graph graph) {
        int n = graph.getN();
        pheromoneGraph = new Graph(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                pheromoneGraph.setEdge(i, j, INITIAL_PHEROMONE_LEVEL);
            }
        }
        for (int i = 0; i < ANTS_NUMBER; i++) {
            tabooArray[i] = new boolean[n][n];
            antPath[i] = new int[n];
        }
    }

    /**
     * Computes probabilityArray values for each index. Rule described in {@link AntColonyAlgorithm#run(Graph)}
     */
    private double[] computeProbabilityArray(int antIndex, int currentCity) {
        double sum = computeEdgesSum(antIndex, currentCity);
        double[] probabilityArray = new double[graph.getN()];
        double position = 0;
        for (int i = 0; i < graph.getN(); i++) {
            double probability;
            if (!tabooArray[antIndex][currentCity][i] && currentCity != i) {

                probability = pow(pheromoneGraph.getEdge(currentCity, i), ALPHA) * pow(1 / graph.getEdge(currentCity, i), BETA) * graph.getN() / sum;

                count++;
            } else {
                probability = 0;
            }
            probabilityArray[i] = position + probability;
            position += probability;
        }
        return probabilityArray;
    }

    /**
     * Saves taboo and path marks
     */
    private void saveMarks(int edgeIndex, int currentCity, int antIndex, int step) {
        for (int k = 0; k < graph.getN(); k++) {// can't move to edgeIndex city anymore
            tabooArray[antIndex][k][edgeIndex] = true;
        }
        antPath[antIndex][step] = edgeIndex;
    }

    private void saveBestWay(int[] bestWay, int[] antWay) {
        for (int i = 0; i < antWay.length; i++) {
            bestWay[i] = antWay[i];
        }
        bestWay[antWay.length] = bestWay[0];
    }

    /**
     * Finds index of an array for which probabilityArray[i] >= x >= probabilityArray[i-1]
     */
    public int binarySearch(double[] probabilityArray, double x) {
        int l = 0;
        int r = probabilityArray.length - 1;
        if (x < probabilityArray[0]) {
            return 0;
        }
        while (!(probabilityArray[(l + r) / 2] >= x && probabilityArray[(l + r) / 2 - 1] <= x)) {
            if (probabilityArray[(l + r) / 2] > x) {
                r = (l + r) / 2;
            } else {
                l = (l + r) / 2 + 1;
            }
        }
        return (l + r) / 2;
    }


    /**
     * Computes sum for probability computation
     */
    private double computeEdgesSum(int antIndex, int currentCity) {
        double result = 0;
        for (int i = 0; i < graph.getN(); i++) {
            if (!tabooArray[antIndex][currentCity][i] && currentCity != i) {
                result += pow(pheromoneGraph.getEdge(currentCity, i), ALPHA) * pow(1 / graph.getEdge(currentCity, i), BETA);
            }
            if (result > 10000000) {
                System.out.println("here");
            }
        }
        return result;
    }

    private void saveIterationResult(int[] bestWay, int iteration) {
        /**
         * Write not more then 1000 iterations
         */
        if (iteration % (ITERATIONS_COUNT / 1000 + 1) == 0 || iteration == ITERATIONS_COUNT - 1) {
            IterationResult iterationResult = new IterationResult();
            iterationResult.setIteration(iteration);
            Cycle cycle = new Cycle(graph.getN(), bestWay);
            cycle.calculateLength(graph);
            iterationResult.setCycle(cycle);
            report.getIterationResults().add(iterationResult);
            report.getBestCycleList().add(cycle);
        }
    }

    private void setReportConstants(Graph graphToRun) {
        report.setAlgorithmName("AntColonyAlgorithm");
        report.getConstants().put("ANTS_NUMBER", ANTS_NUMBER);
        report.getConstants().put("INITIAL_PHEROMONE_LEVEL", INITIAL_PHEROMONE_LEVEL);
        report.getConstants().put("ITERATIONS_COUNT", ITERATIONS_COUNT);
        report.getConstants().put("ALPHA", ALPHA);
        report.getConstants().put("BETA", BETA);
        report.getConstants().put("Q", Q);
        report.getConstants().put("P", P);
        report.setGraphName(graphToRun.getName());
        report.setGraphDescription(graphToRun.getDescription());
    }

    class AntTravelCallable implements Callable<Boolean> {
        int antIndex;

        public AntTravelCallable(int antIndex) {
            this.antIndex = antIndex;
        }

        @Override
        public Boolean call() throws Exception {
            return antTravel(antIndex);
        }
    }
}
