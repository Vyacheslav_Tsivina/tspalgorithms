package algorithms;

import beans.Cycle;
import beans.Graph;
import beans.IterationResult;
import beans.Report;
import logger.Logger;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static util.AnnealingConstants.*;
import static util.CommonGraphOperations.cycleLength;
import static util.CommonGraphOperations.logNewBestCycle;

public class SimulatedAnnealingAlgorithm {
    private Logger logger;
    private Graph graph;
    private Report report;
    public SimulatedAnnealingAlgorithm(Logger logger) {
        this.logger = logger;
        report = new Report();
    }

    public Report run(Graph graphToRun) {
        report.setGraphName(graphToRun.getName());
        report.setGraphDescription(graphToRun.getDescription());
        report.setAlgorithmName("SimulatedAnnealingAlgorithm");
        report.getConstants().put("ITERATIONS_COUNT",ITERATIONS_COUNT);
        report.getConstants().put("INITIAL_TEMPERATURE",INITIAL_TEMPERATURE);
        report.getConstants().put("VERTEXES_TO_CHANGE",VERTEXES_TO_CHANGE);
        report.getConstants().put("TEMPERATURE_FUNCTION",TEMPERATURE_FUNCTION.getClass().toString());

        long startTime = new Date().getTime();
        graph = graphToRun;
        int[] currentCycle = generateCycle(graph.getN());
        Random rand = new Random();
        for (int iteration = 0; iteration < ITERATIONS_COUNT; iteration++) {
            int[] candidateCycle = new int[graph.getN() + 1];

            for (int i = 0; i < graph.getN() + 1; i++) {
                candidateCycle[i] = currentCycle[i];
            }
            for (int i = 0; i < VERTEXES_TO_CHANGE; i++) {
                swapVertexesInCycle(candidateCycle);
            }
            if (cycleLength(candidateCycle, graph) < cycleLength(currentCycle, graph)) {
                currentCycle = candidateCycle;
            } else {
                double currentCycleLength = cycleLength(currentCycle, graph);
                double candidateCycleLength = cycleLength(candidateCycle, graph);

                double probability = probability(
                        candidateCycleLength - currentCycleLength,
                        TEMPERATURE_FUNCTION.temperature(iteration));
                if (iteration %10000 == 0){
                    logger.logDebug("Probability : "+probability);
                }
                if (rand.nextDouble() < probability
                        && (Math.abs(candidateCycleLength - currentCycleLength)) > 0.0001) {
                    double e = candidateCycleLength - currentCycleLength;
                    currentCycle = candidateCycle;
                    logNewBestCycle(currentCycle, iteration, cycleLength(currentCycle, graph), logger, graph);
                    logger.logDebug("CHANGED BY PROBABILITY");
                    logger.logDebug("PROB WAS = " + probability);
                    logger.logDebug("E = " + e);
                    logger.logDebug("T = " + TEMPERATURE_FUNCTION.temperature(iteration));
                }
            }
            saveIterationResult(currentCycle, iteration);
            if ( new Date().getTime() - startTime > WAIT_UNTIL_STOP){//each graph has no more than hour
                report.getConstants().put("INTERRUPTED","TRUE");
                break;
            }
        }
        long endTime = new Date().getTime();
        logger.logString("----------------------------------------------------------------------");
        logger.logString("Run method execution time:" + (endTime - startTime) + " ms");
        logger.logPath(currentCycle, cycleLength(currentCycle, graph));
        report.setTime(endTime - startTime);
        return report;
    }

    /**
     * For bigger dE we have smaller probability. When temperature tends to zero
     * probapility tends to zero too
     *
     * @param dE new cycle length - current cycle length
     * @param t temperature
     * @return probability to change cycle
     */
    private double probability(double dE, double t) {
        return Math.exp(-dE / t);
    }


    /**
     * Generates random cycle where first and last elements are equals
     *
     * @param n cycle length
     * @return cycle
     */
    private int[] generateCycle(int n) {
        int[] result = new int[n + 1];
        Random random = new Random();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            map.put(i, i);
        }
        for (int i = 0; i < n; i++) {
            int tmp = random.nextInt(n - i);
            result[i] = map.get(tmp);
            map.put(tmp, map.get(n - i - 1));
        }
        result[n] = result[0];
        return result;
    }

    private void swapVertexesInCycle(int[] cycle) {
        Random rand = new Random();
        int x = rand.nextInt(graph.getN() - 1) + 1;// don't include 0 because it's cycle
        int y = rand.nextInt(graph.getN() - 1) + 1;
        while (x == y) {
            y = rand.nextInt(graph.getN() - 1) + 1;
        }
        int tmp = cycle[x];
        cycle[x] = cycle[y];
        cycle[y] = tmp;
    }

    private void saveIterationResult(int[] bestWay, int iteration) {
        /**
         * Write not more then 1000 iterations
         */
        if (iteration % (ITERATIONS_COUNT / 1000 +1) == 0 || iteration == ITERATIONS_COUNT - 1) {
            IterationResult iterationResult = new IterationResult();
            iterationResult.setIteration(iteration);
            Cycle cycle = new Cycle(graph.getN(), bestWay);
            cycle.calculateLength(graph);
            iterationResult.setCycle(cycle);
            report.getIterationResults().add(iterationResult);
            report.getBestCycleList().add(cycle);
        }
    }
}
