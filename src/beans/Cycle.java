package beans;

import util.CommonGraphOperations;

import java.io.Serializable;
import java.util.Arrays;

public class Cycle implements Serializable{

    private static final long serialVersionUID = 4257083417196256794L;

    private int[] vertexes;
    private int n;
    private double length;

    public Cycle(int n, int[] vertexes){
        this.n = n;
        this.vertexes = vertexes;
    }

    public double calculateLength(Graph graph){
        length = CommonGraphOperations.cycleLength(vertexes, graph);
        return length;
    }

    public int[] getVertexes() {
        return vertexes;
    }

    public void setVertexes(int[] vertexes) {
        this.vertexes = vertexes;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void swapVertexes(int i, int j, Graph graph){
        int tmp = vertexes[i];
        vertexes[i] = vertexes[j];
        vertexes[j] = tmp;
    }

    @Override
    public String toString() {
        return "Cycle{" +
                "\nvertexes=" + Arrays.toString(vertexes) +
                ",\n n=" + n +
                ",\n length=" + length +
                "\n}";
    }
}
