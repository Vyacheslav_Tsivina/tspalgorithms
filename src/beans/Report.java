package beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Report implements Serializable {

    private static final long serialVersionUID = 9035927681594139817L;
    private String graphName;
    private String graphDescription;
    private String algorithmName;
    private long time;
    private List<Cycle> bestCycleList = new LinkedList<>();
    Map<String, Object> constants = new HashMap<>();
    List<IterationResult> iterationResults = new LinkedList<>();

    public String getGraphName() {
        return graphName;
    }

    public void setGraphName(String graphName) {
        this.graphName = graphName;
    }

    public String getGraphDescription() {
        return graphDescription;
    }

    public void setGraphDescription(String graphDescription) {
        this.graphDescription = graphDescription;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<Cycle> getBestCycleList() {
        return bestCycleList;
    }

    public void setBestCycleList(List<Cycle> bestCycleList) {
        this.bestCycleList = bestCycleList;
    }

    public Map<String, Object> getConstants() {
        return constants;
    }

    public void setConstants(Map<String, Object> constants) {
        this.constants = constants;
    }

    public List<IterationResult> getIterationResults() {
        return iterationResults;
    }

    public void setIterationResults(List<IterationResult> iterationResults) {
        this.iterationResults = iterationResults;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Report{");
        sb.append("graphName='").append(graphName).append("'\n");
        sb.append(", graphDescription='").append(graphDescription).append("'\n");
        sb.append(", algorithmName='").append(algorithmName).append("'\n");
        sb.append(", time=").append(time).append("'\n");
        sb.append(", constants=").append(constants).append("'\n");
        sb.append(", iterationResults=").append(iterationResults).append("'\n");
        sb.append('}');
        return sb.toString();
    }
}
