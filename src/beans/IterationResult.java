package beans;

import java.io.Serializable;

public class IterationResult implements Serializable {

    private static final long serialVersionUID = 5726078784363501786L;

    private int iteration;


    private Cycle cycle;

    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }

    @Override
    public String toString() {
        return "IterationResult{" +
                "iteration=" + iteration +
                ",\n cycle=" + cycle +
                '}';
    }
}
