package beans;

import java.io.Serializable;

public class Graph implements Serializable{

    private static final long serialVersionUID = 7637776906242984625L;

    private double[][] adjacencyMatrix;
    private String name;
    private String description;

    public Graph(int n){
        adjacencyMatrix = new double[n][n];
    }

    public void setEdge(int i,int j,double c){
        adjacencyMatrix[i][j] = c;
    }

    public double getEdge(int i, int j){
        return adjacencyMatrix[i][j];
    }

    public int getN(){
        return adjacencyMatrix.length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
