package main;

import algorithms.AntColonyAlgorithm;
import algorithms.SimulatedAnnealingAlgorithm;
import beans.Graph;
import beans.Report;
import logger.impl.ConsoleLogger;
import logger.impl.FileLogger;
import util.*;
import util.readers.XMLGraphReader;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    public static void main(String[] args) throws Exception {
        runParallelTest();
        // runParallelTest();
 //       comparing();
        //System.out.println(CommonGraphOperations.wayLength(way, graph));
        //GraphGenerator.generateIntGraph(fileName, 30,100);
        //File examplesdir = new File("log");
//        Pattern p = Pattern.compile("([a-z|\\d]+)\\-?(\\d*)\\-?(AC|SA).txt");
//        for (File file : examplesdir.listFiles()) {
//            Matcher m = p.matcher(file.getName());
//            if (m.find()) {
//                String testName = m.group(1);
//                String attemptStr = m.group(2);
//                String method = m.group(3);
//                int attempt = CommonUtils.parseIntOrDefault(attemptStr, 0);
//                if (attempt == 0){
//                    file.renameTo(new File("log/"+testName+"-"+(attempt+1)+"-"+method+".txt"));
//                }
//            }
//        }
    }

    public static void comparing() throws Exception {
        File examplesdir = new File("log");
        Pattern p = Pattern.compile("([a-z|\\d]+)\\-?(\\d*)\\-?(AC|SA).txt");
        Map<String, List<Pair<Integer, Double>>> examples = new LinkedHashMap<>();
        Set<String> testNameList = new LinkedHashSet<>();
        for (File file : examplesdir.listFiles()) {

            Matcher m = p.matcher(file.getName());
            if (m.find()) {
                String testName = m.group(1);
                String attemptStr = m.group(2);
                String method = m.group(3);
                int attempt = CommonUtils.parseIntOrDefault(attemptStr, 0);
                testNameList.add(testName);
                List<Pair<Integer, Double>> testExamples = examples.get(testName + method + attempt);
                if (testExamples == null) {
                    testExamples = new ArrayList<>();
                }
                List<String> report = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
                Pattern time = Pattern.compile("time:(\\d+)");
                Matcher timeMatch;
                Pair<Integer, Double> pair = new Pair<>();
                for (String str : report) {
                    timeMatch = time.matcher(str);
                    if (timeMatch.find()) {
                        pair = new Pair<>(-1, (double) CommonUtils.parseLongOrDefault(timeMatch.group(1), -1L));
                        testExamples.add(pair);
                        break;
                    }
                }
                Pattern iterationPattern = Pattern.compile("iteration=(\\d+)");
                Pattern lengthPattern = Pattern.compile("length=([\\d|\\.]+)");
                for (String str : report) {
                    if (str.length() > 100) {
                        continue;
                    }
                    Matcher mi = iterationPattern.matcher(str);
                    if (mi.find()) {
                        pair = new Pair<>();
                        pair.setLeft(Integer.valueOf(mi.group(1)));
                        continue;
                    }
                    Matcher ml = lengthPattern.matcher(str);
                    if (ml.find()) {
                        pair.setRight(Double.valueOf(ml.group(1)));
                        testExamples.add(pair);
                    }
                }
                examples.put(testName + method + attempt, testExamples);
                File csvFile = new File("datasets/" + file.getName().replace("txt", "csv"));
                if (!csvFile.exists()) {
                    csvFile.createNewFile();
                    System.out.println("Create " + csvFile.getName());
                    for (Pair<Integer, Double> csvPair : testExamples) {
                        if (csvPair.getLeft().equals(-1)) continue;
                        Files.write(csvFile.toPath(), (csvPair.getLeft() + "," + csvPair.getRight() + "\n").getBytes(), StandardOpenOption.APPEND);
                    }
                }
            }
        }

        //findWorseAttempt(examples,testNameList,"AC");
        //findWorseAttempt(examples,testNameList,"SA");
    }

    /**
     * All attempts have bigger number of iterations. This method fings if the method proposed slution that is worse than in previous attempts
     *
     * @param examples
     * @param testNameList
     */
    private static void findWorseAttempt(Map<String, List<Pair<Integer, Double>>> examples, Set<String> testNameList, String method) {
        for (String testName : testNameList) {
            int attempt = 1;
            List<Pair<Integer, Double>> result = examples.get(testName + method + attempt);
            int bestAttemptNumber = 1;
            double bestAttemptLength = result.get(result.size() - 1).getRight();
            while (result != null) {

                if (bestAttemptLength < result.get(result.size() - 1).getRight()) {
                    System.out.println("В тесте " + testName + " результат попытки " + attempt +
                            "(" + result.get(result.size() - 1).getRight() + ") хуже чем для " + bestAttemptNumber + "(" + bestAttemptLength + ") в методе " + method);
                } else {
                    bestAttemptLength = result.get(result.size() - 1).getRight();
                    bestAttemptNumber = attempt;
                }
                attempt++;
                result = examples.get(testName + method + attempt);
            }
        }
    }

    public static void runParallelTest() throws Exception {
        File example = new File("XML_examples/d493.xml");
        Graph graph = XMLGraphReader.readGraph(example);
        ConsoleLogger consoleLogger = new ConsoleLogger();
        AntColonyConstants.ANTS_NUMBER = 40;
        AntColonyConstants.ITERATIONS_COUNT = 250;
        AntColonyAlgorithm algorithm = new AntColonyAlgorithm(consoleLogger);
        System.out.println("DEFAULT PARAMETERS");
        algorithm.runParallel(graph);
          AntColonyConstants.ALPHA = 2;
        AntColonyConstants.BETA = 2;
        System.out.println("ALPHA=2, BETA=2");
        algorithm = new AntColonyAlgorithm(consoleLogger);
        algorithm.runParallel(graph);
        System.out.println("ALPHA=2, BETA=2, P=0.3");
        AntColonyConstants.P = 0.3;
        algorithm = new AntColonyAlgorithm(consoleLogger);
        algorithm.runParallel(graph);
        System.out.println("ALPHA=2, BETA=1");
        AntColonyConstants.BETA = 1;
        algorithm = new AntColonyAlgorithm(consoleLogger);
        algorithm.runParallel(graph);
        System.out.println("ALPHA=1, BETA=2");
        AntColonyConstants.BETA = 2;
        AntColonyConstants.ALPHA = 1;
        algorithm = new AntColonyAlgorithm(consoleLogger);
        algorithm.runParallel(graph);
//        consoleLogger.logString("");
//        AntColonyConstants.P = 0.3;
//        System.out.println("P=0.3");
//        algorithm = new AntColonyAlgorithm(consoleLogger);
//        report = algorithm.runParallel(graph);
//        ph = (List<Double>)report.getConstants().get("PHEROMONE_AMOUNT");
//        for (int i = 0; i < ph.size(); i++){
//            if (i%10 == 0){
//                consoleLogger.logString(i+","+ph.get(i)*100);
//            }
//        }
//        consoleLogger.logString("");
//        AntColonyConstants.P = 0.25;
//        System.out.println("P=0.25");
//        algorithm = new AntColonyAlgorithm(consoleLogger);
//        report = algorithm.runParallel(graph);
//        ph = (List<Double>)report.getConstants().get("PHEROMONE_AMOUNT");
//        for (int i = 0; i < ph.size(); i++){
//            if (i%10 == 0){
//                consoleLogger.logString(i+","+ph.get(i)*100);
//            }
//        }
    }

    /**
     * Runs algorithm on all graphs from 'XML_examples' directory. Change constans according to graph size
     *
     * @throws Exception
     */
    public static void runTest() throws Exception {
        File examplesdir = new File("XML_examples");
        ConsoleLogger consoleLogger = new ConsoleLogger();
        for (File example : examplesdir.listFiles()) {
            int i = 1;
            File logFileAC;
            while (true) {
                logFileAC = new File("log/" + example.getName().replaceFirst("[.][^.]+$", "-" + i + "-AC.txt"));
                i++;
                if (!logFileAC.exists()) {
                    logFileAC.createNewFile();
                    break;
                }
            }
            i = 1;
            File logFileSA;
            while (true) {
                logFileSA = new File("log/" + example.getName().replaceFirst("[.][^.]+$", "-" + i + "-SA.txt"));
                i++;
                if (!logFileSA.exists()) {
                    logFileSA.createNewFile();
                    break;
                }
            }
            FileLogger fileLoggerAC = new FileLogger(logFileAC);
            FileLogger fileLoggerSA = new FileLogger(logFileSA);
            System.out.println("FILE " + example.getName());
            Graph graph = XMLGraphReader.readGraph(example);
            AnnealingConstants.INITIAL_TEMPERATURE = graph.getN();
            if (graph.getN() < 100) {
                AnnealingConstants.ITERATIONS_COUNT = 120_000_000;
                AnnealingConstants.VERTEXES_TO_CHANGE = 2;

                AntColonyConstants.ANTS_NUMBER = 300;
                AntColonyConstants.ITERATIONS_COUNT = 1000;
            }

            if (graph.getN() < 1000) {
                AnnealingConstants.ITERATIONS_COUNT = 160_000_000;
                AnnealingConstants.VERTEXES_TO_CHANGE = 3;

                AntColonyConstants.ANTS_NUMBER = 100;
                AntColonyConstants.ITERATIONS_COUNT = 1430;
            }

            if (graph.getN() > 1000) {
                AnnealingConstants.ITERATIONS_COUNT = 90_000_000;
                AnnealingConstants.VERTEXES_TO_CHANGE = 4;

                AntColonyConstants.ANTS_NUMBER = 80;
                AntColonyConstants.ITERATIONS_COUNT = 700;
            }

            AntColonyAlgorithm antColonyAlgorithm = new AntColonyAlgorithm(fileLoggerAC);
            SimulatedAnnealingAlgorithm simulatedAnnealingAlgorithm = new SimulatedAnnealingAlgorithm(fileLoggerSA);
            Report report = antColonyAlgorithm.run(graph);
            fileLoggerAC.logString(report.toString());
            consoleLogger.logString("LENGTH AC= " + report.getBestCycleList().get(report.getBestCycleList().size() - 1).getLength());
            report = simulatedAnnealingAlgorithm.run(graph);
            fileLoggerSA.logString(report.toString());
            consoleLogger.logString("LENGTH SA= " + report.getBestCycleList().get(report.getBestCycleList().size() - 1).getLength());
            Thread.sleep(1500);
            fileLoggerAC.closeBuffer();
            fileLoggerSA.closeBuffer();
        }
    }
}
