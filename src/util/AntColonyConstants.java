package util;

public class AntColonyConstants {
    /**
     * Number of ants
     */
    public static int ANTS_NUMBER = 40;
    /**
     * Initial pheromone level
     */
    public static double INITIAL_PHEROMONE_LEVEL = 0.1;
    /**
     * Iterations count
     */
    public static int ITERATIONS_COUNT = 100;

    /**
     * Alpha coefficient for probability function
     */
    public static double ALPHA = 1;
    /**
     * Beta coefficient for probability function
     */
    public static double BETA = 1;
    /**
     * Accuracy of calculations
     */
    public static double EPS = 0.0001;
    /**
     * Constant used in pheromone evaporation calculation
     */
    public static double Q = 1;
    /**
     * Constant used in pheromone evaporation calculation
     */
    public static double P = 0.2;

    /**
     * Maximum algorithm work time
     * Algorithm will stopped, report will be returned
     */
    public static long WAIT_UNTIL_STOP = 3*3600*1000;
}
