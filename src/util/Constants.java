package util;

import java.text.SimpleDateFormat;

public class Constants {
    /**
     * Log level
     */
    public static final LogLevel LOG_LEVEL = LogLevel.RUN;

    public static final boolean ADDITIONAL_INFO = true;
    public enum LogLevel{
        DEBUG, RUN
    }

    public static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("hh:mm:ss");
}
