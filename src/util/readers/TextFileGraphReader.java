package util.readers;

import beans.Graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextFileGraphReader {
    private TextFileGraphReader(){}
    public static Graph readGraph( String fileName){
        File file = new File(fileName);
        try {
            Scanner sc = new Scanner(file);
            int n = sc.nextInt();
            Graph result = new Graph(n);
            for (int i=0;i<n; i++)
                for (int j=0; j<n; j++)
                    result.setEdge(i,j, sc.nextDouble());
            return result;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
