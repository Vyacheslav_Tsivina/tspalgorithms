package util.readers;

import beans.Graph;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class XMLGraphReader {
    private static final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    //DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    public static Graph readGraph(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = db.parse(file);
        NodeList list = doc.getChildNodes();

        int vertexesCount = doc.getElementsByTagName("vertex").getLength();
        Graph result = new Graph(vertexesCount);
        result.setName(doc.getElementsByTagName("name").item(0).getTextContent());
        result.setDescription(doc.getElementsByTagName("description").item(0).getTextContent());
        NodeList vertexes = doc.getElementsByTagName("vertex");
        for (int i = 0; i < vertexesCount; i++){
            Node vertex =  vertexes.item(i);
            int childCount = vertex.getChildNodes().getLength();
            for (int j = 0; j < childCount; j++){
                Node edge = vertex.getChildNodes().item(j);
                if (!"edge".equals(edge.getNodeName())){
                    continue;
                }
                double cost = Double.valueOf(edge.getAttributes().getNamedItem("cost").getTextContent());
                if ( cost < 0.0001){
                    cost = 0.01;
                }
                result.setEdge(i, Integer.valueOf(edge.getTextContent()), cost);
            }
        }
        return result;
    }

}
