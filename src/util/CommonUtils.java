package util;

/**
 * Class for common util methods
 */
public class CommonUtils {


    private CommonUtils() {
    }

    /**
     * check value and return value or default value if value is null
     *
     * @param val          value to
     * @param defaultValue returned value if {@param val} is null
     * @return {@param val} if not null or {@param defaultValue} if {@param val} is null
     */
    public static <T> T getOrDefault(T val, T defaultValue) {
        return val != null ? val : defaultValue;
    }

    /**
     * Tries to parse long from string and returns default value if fails
     *
     * @param val          value to parse
     * @param defaultValue returned value if failed to parse {@param val}
     * @return Long if successfully parsed or default value if fails
     */
    public static Long parseLongOrDefault(String val, Long defaultValue) {
        try {
            return Long.valueOf(val);
        } catch (NumberFormatException e) {
            System.err.println(e);
        }
        return defaultValue;
    }

    /**
     * Tries to parse int from string and returns default value if fails
     *
     * @param val          value to parse
     * @param defaultValue returned value if failed to parse {@param val}
     * @return Integer if successfully parsed or default value if fails
     */
    public static Integer parseIntOrDefault(String val, Integer defaultValue) {
        try {
            return Integer.valueOf(val);
        } catch (NumberFormatException e) {
            System.err.println(e);
        }
        return defaultValue;
    }
}
