package util;

public class AnnealingConstants {
    public static long ITERATIONS_COUNT = 30000000;
    public static double INITIAL_TEMPERATURE = 40;
    public static long VERTEXES_TO_CHANGE = 2;
    public static TemperatureFunc TEMPERATURE_FUNCTION = new LinearTemperatureFunction();

    public interface TemperatureFunc{
        /**
     * Temperature function
     * @param i iteration
     * @return current temperature
     */
        double temperature(int i);
    }

    /**
     * Maximum algorithm work time
     * Algorithm will stopped, report will be returned
     */
    public static long WAIT_UNTIL_STOP = 3*3600*1000;

    public static class LinearTemperatureFunction implements TemperatureFunc{
        @Override
        public double temperature(int i) {
            double a = (0.001-INITIAL_TEMPERATURE)/(ITERATIONS_COUNT-1);
            double b = INITIAL_TEMPERATURE - a;
            return a*i+b;
        }
    }

    public static class QuadraticTemperatureFunction implements TemperatureFunc{
        @Override
        public double temperature(int i) {
            double a = INITIAL_TEMPERATURE/Math.sqrt(ITERATIONS_COUNT);
            return INITIAL_TEMPERATURE-a*Math.sqrt(i)+0.0001;
        }
    }

    public static class LogarithmTemperatureFunction implements TemperatureFunc{
        @Override
        public double temperature(int i) {
            double a = INITIAL_TEMPERATURE/Math.log(ITERATIONS_COUNT);
            return INITIAL_TEMPERATURE-a*Math.log(i+0.0001)+0.0001;
        }
    }
}
