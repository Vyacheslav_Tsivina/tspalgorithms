package util;

import logger.Logger;
import beans.Graph;

public class CommonGraphOperations {
    public static double cycleLength(int[] cycle, Graph graph){
        double result = 0;
        for (int i = 0; i<graph.getN(); i++){
            result+=graph.getEdge(cycle[i],cycle[i+1]);
        }
        return result;
    }

    public static double wayLength(int[] way, Graph graph){
        double result = 0;
        for (int i = 0; i<graph.getN()-1; i++){
            result+=graph.getEdge(way[i],way[i+1]);
        }
        return result;
    }

    /**
     * Print bestWay only for DEBUG level
     * @param bestWay
     * @param iteration
     * @param bestWayLength
     * @param logger
     * @param graph
     */
    public static void logNewBestCycle(int[] bestWay, int iteration, double bestWayLength, Logger logger, Graph graph) {
        logger.logDebug(String.format("New best way! Iteration %d. Way length = %.3f", iteration, bestWayLength));
        StringBuilder message = new StringBuilder();
        message.append("[");
        message.append(bestWay[0]);
        for (int i = 1; i <= graph.getN(); i++) {
            message.append(", " + bestWay[i]);
        }
        message.append("]");
        logger.logDebug(message.toString());
        logger.logDebug("");
    }


}
